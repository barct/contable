# -*- coding: utf-8 -*-
from decimal import Decimal
from django import forms
from django.core import exceptions
from django.db import models
import json


class PartidaDoble():

    def __init__(self, debe=0, haber=0):
        if debe is not None:
            self.debe = Decimal(debe)
        else:
            self.debe = Decimal(0)

        if haber is not None:
            self.haber = Decimal(haber)
        else:
            self.haber = Decimal(0)

    def __add__(self, val):
        return PartidaDoble(self.debe + val.debe, self.haber + val.haber)

    def __iadd__(self, pd):

        self.debe += pd.debe
        self.haber += pd.haber
        return self

    def __eq__(self, pd):
        return (self.debe == pd.debe and self.haber == pd.haber)

    @property
    def resultado(self):
        return self.debe - self.haber

    @property
    def saldo(self):
        if self.debe > self.haber:
            return PartidaDoble(self.debe - self.haber, 0)
        else:
            return PartidaDoble(0, self.haber - self.debe)

    @property
    def isZero(self):
        return (self.debe == Decimal(0) and self.haber == Decimal(0))

    def __str__(self):
        return "D: %d H: %d R:%d" % (self.debe, self.haber, self.resultado)

    def __unicode__(self):
        return self.__str__()


class DictionaryField(models.Field):

    def get_internal_type(self):
        return "TextField"

    def to_python(self, value):
        if value is None:
            return None
        elif value == "":
            return {}
        elif isinstance(value, basestring):
            try:
                return dict(json.loads(value))
            except (ValueError, TypeError):
                raise exceptions.ValidationError(
                    self.error_messages['invalid'])

        if isinstance(value, dict):
            return value
        else:
            return {}

    def get_prep_value(self, value):
        if not value:
            return ""
        elif isinstance(value, basestring):
            return value
        else:
            return json.dumps(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_prep_value(value)

    def clean(self, value, model_instance):
        value = super(DictionaryField, self).clean(value, model_instance)
        return self.get_prep_value(value)

    def formfield(self, **kwargs):
        defaults = {'widget': forms.Textarea}
        defaults.update(kwargs)
        return super(DictionaryField, self).formfield(**defaults)
