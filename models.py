# -*- coding: utf-8 *-*
from django.db import models
from django.core.exceptions import ValidationError
from decimal import Decimal
from django.db.models import Sum, Count, Q
import utils
import warnings
from django.utils.deprecation import RemovedInNextVersionWarning


class Ejercicio(models.Model):
    """representa un ejercicio contable"""
    class Meta:
        # solo puede haber una cuenta por ejercicio
        permissions = (
            ("view_ejercicio", "Puede ver los ejercicios"),)

    id = models.PositiveIntegerField("Número", primary_key=True)
    fecha_inicio = models.DateField("Fecha de inicio")
    fecha_fin = models.DateField("Fecha de final")
    ano = models.PositiveIntegerField("Año")

    activo = models.BooleanField("Activo", default=False)

    def __unicode__(self):
        return str(self.ano)

    def save(self, *args, **kwargs):
        # Verifico que sea el único ejercicio activo
        e_activo = Ejercicio.objects.filter(activo=True)
        if e_activo.count() > 0:
            e_activo = e_activo[0]
        else:
            self.activo = True
            e_activo = self

        change_eactivo = False
        if self.activo is True and e_activo != self:
            e_activo.activo = False
            change_eactivo = True

        super(Ejercicio, self).save(*args, **kwargs)
        if change_eactivo:
            e_activo.save()

    def get_next_asiento_numero(self):
        """si no le asigno numero, que asigne el suiguiente"""
        asientos = self.asiento_set.order_by("-numero")
        if asientos.count() > 0:
            numero = asientos[0].numero + 1
        else:
            numero = 1

        return numero

    def resort_asiento_numero(self):
        asientos = self.asiento_set.order_by("imputacion", "numero")
        index = 1
        for asiento in asientos:
            asiento.numero = index
            asiento.save()
            index += 1

    def get_others(self):
        return Ejercicio.objects.exclude(pk=self.pk)

    def get_rel_cuentas_ids_from_codigos(self, codigos):
        ids = list()
        cuentas = self.cuenta_set.filter(codigo__in=codigos)
        for c in cuentas:
            ids += c.get_rel_ids()
        return ids

    @staticmethod
    def last():
        ejes = Ejercicio.objects.all().order_by("-ano")
        return ejes[0]

    @staticmethod
    def get_activo():
        ejs = Ejercicio.objects.order_by('-activo', '-ano')
        if ejs.count() > 0:
            if ejs[0].activo is False:
                ejs[0].activo = True
                ejs[0].save()
            return ejs[0]
        else:
            return None

    @staticmethod
    def get_ejercicio_by_date(date):
        ejs = Ejercicio.objects.filter(
            fecha_inicio__lte=date, fecha_fin__gte=date)
        if ejs.count() == 1:
            return ejs[0]
        elif ejs.count() > 1:
            raise Exception("Ejercicios superpuestos")
        else:
            return None


class CuentaKey(str):
    max_length = 13
    estruct = 5

    def __new__(cls, value):
        return str.__new__(cls, CuentaKey.NormalizeKey(value))

    def get_element(self, index):
        estructura = self.split(".")
        return int(estructura[index])

    def set_element(self, index, value):
        estructura = self.split(".")
        estructura[index] = value
        self = CuentaKey.NormalizeKey(str.join(".", estructura))

    @property
    def parent(self):
        """obtiene el codigo de la cuenta padre"""
        estructura = self.split(".")
        parent = list((0, 0, 0, 0, 0))
        parent_part = False

        for i in (4, 3, 2, 1, 0):
            e = estructura[i]
            if not parent_part:
                parent[i] = "0"
            else:
                parent[i] = e
            if e not in ("0", "00", "000"):
                parent_part = True
        return CuentaKey.NormalizeKey(str.join(".", parent))

    @staticmethod
    def check_key(key):
        """Chequea si el cógigo tiene el formato N.N.NN.NN.NNN"""
        try:
            key = CuentaKey.NormalizeKey(key)
        except (ValueError, IndexError):
            return False
        if len(key) != CuentaKey.max_length:
            return False
        estructura = key.split(".")
        if len(estructura) != CuentaKey.estruct:
            return False
        return True

    @staticmethod
    def NormalizeKey(key):
        """Toma un codigo y lo formatea (si puede) en un codigo de cuenta"""
        estructura = key.split(".")
        for i in (0, 1, 2, 3, 4):
            if i == 2 or i == 3:
                estructura[i] = str(int(estructura[i])).rjust(2, "0")
            elif i == 4:
                estructura[i] = str(int(estructura[i])).rjust(3, "0")
            else:
                estructura[i] = str(int(estructura[i]))
        return str.join(".", estructura)

    @staticmethod
    def ZeroValue():
        return CuentaKey("0.0.00.00.000")


class CuentaKeyField(models.CharField):

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = CuentaKey.max_length
        super(CuentaKeyField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if value is None or isinstance(value, CuentaKey):
            return value

        if CuentaKey.check_key(value):
            return CuentaKey(value)
        else:
            raise ValidationError(
                "El valor no corresponde con un código de cuenta válido.")


class Cuenta(models.Model):
    """Representa una cuenta contable"""

    class Meta:
        # solo puede haber una cuenta por ejercicio
        unique_together = (('codigo', 'ejercicio'),)
        permissions = (
            ("view_plan_de_cuentas", "Puede ver los planes de cuentas"),)

    codigo = CuentaKeyField("Código")
    ejercicio = models.ForeignKey(Ejercicio)

    breve = models.CharField(
        "Acceso Rápido", max_length=5, null=True, blank=True)
    nombre = models.CharField("Nombre", max_length=127)
    imputable = models.BooleanField(default=False)

    parent = models.ForeignKey('Cuenta', null=True, blank=True)

    def save(self, *args, **kwargs):
        if isinstance(self.codigo, str):
            self.codigo = CuentaKey(self.codigo)
        parent_codigo = self.codigo.parent
        if parent_codigo != CuentaKey.ZeroValue():
            self.parent, c = Cuenta.objects.get_or_create(
                codigo=parent_codigo,
                ejercicio=self.ejercicio,
                defaults={'nombre': "(A DEFINIR)", 'imputable': False})

        super(Cuenta, self).save(*args, **kwargs)

    def get_codigo_siguiente(self):
        """obtiene el nuevo codigo de una sihguiente cuenta hija"""
        subcuentas = self.cuenta_set.order_by("-codigo")
        if subcuentas.count() > 0:
            nivel = subcuentas[0].nivel
            bloque = subcuentas[0].codigo.split(".")
            index = int(bloque[nivel])
            index += 1
            bloque[nivel] = str(index)
        else:
            bloque = self.codigo.split(".")
            bloque[self.nivel + 1] = "1"
        return CuentaKey.NormalizeKey(".".join(bloque))

    def get_saldo(self, recursive=False, fecha_fin=None, exclude=list()):
        """ retorna el saldo de la cuenta sumarizando
        los valores de los asientos
        recursive = True para profundizar la busqueda
        de saldo de manera recursiva
        fecha_fin: Hasta cierta fecha
        exclude: tipo de asiento excluidos 0,1,2,3,4
        """

        if hasattr(self, "__saldo") and not fecha_fin:
            return self.__saldo
        elif (hasattr(self, "__saldo_fecha") and
              fecha_fin in self.__saldo_fecha):
            return self.saldo_fecha[fecha_fin]
        else:
            if hasattr(self, 'debitos') and hasattr(self, 'creditos'):
                saldo = utils.PartidaDoble(self.debitos, self.creditos)
            else:

                q_filter = dict()
                q_exclude = dict()
                if fecha_fin:
                    q_filter['asiento__imputacion__lte'] = fecha_fin
                if exclude:
                    q_exclude['asiento__tipo__in'] = exclude

                asientos = self.registroasiento_set.filter(
                    **q_filter).exclude(**q_exclude)
                saldo = utils.PartidaDoble(0, 0)
                for reg in asientos:
                    saldo += reg.saldo
            if recursive:
                subcuentas = self.cuenta_set.annotate(
                    debitos=Sum("registroasiento__debe"),
                    creditos=Sum("registroasiento__haber"))

                for sc in subcuentas:
                    print sc
                    saldo = saldo + sc.get_saldo(recursive, fecha_fin, exclude)
        self.__saldo = saldo
        return saldo

    @property
    def saldo(self):
        """expone __saldo__ de manera recursiva"""
        return self.get_saldo()

    @property
    def full_saldo(self):
        """expone __saldo__ de manera recursiva de todas las cuentas"""
        return self.get_saldo(recursive=True)

    @property
    def nivel(self, recalculate=False):
        """determina el nivel de la cuenta y guarda en cache

            recalculate = True si queremos que actualize el cache
        """
        if not hasattr(self, "__nivel") or recalculate:
            self.__nivel = 4
            estructura = self.codigo.split(".")
            estructura.reverse()
            for bloque in estructura:
                if int(bloque) == 0:
                    self.__nivel -= 1
        return self.__nivel

    def get_movimientos(self):
        ids = self.get_rel_ids()
        return RegistroAsiento.objects.filter(cuenta_id__in=ids)

    def get_rel_cuentas(self):
        ids = self.get_rel_ids()
        return Cuenta.objects.filter(id__in=ids)

    _rel_ids = False

    def get_rel_ids(self, rebuild=False):
        if not self._rel_ids or rebuild:
            def make_id_list(cuenta, list):
                list.append(cuenta.id)
                subcuentas = cuenta.cuenta_set.all()
                for sc in subcuentas:
                    make_id_list(sc, list)

            def getParents(cuenta, list):
                if cuenta.parent is not None:
                    list.append(cuenta.parent)
                    getParents(cuenta.parent, list)

            parent_list = list()
            getParents(self, parent_list)
            parent_list.reverse()
            self.parent_list = parent_list

            ids = list()
            make_id_list(self, ids)
            self._rel_ids = ids
        return self._rel_ids

    @staticmethod
    def listadoCompleto(ejercicio_id, hide_inuse=False, ids=None,
                        fecha_fin=None, tipo__in=None):
        """ Arma el listado de cuentas (plan de cuetas) sumarizando los
        saldos evitando la recursividad """
        # filtro ejercicio y si vienen ids, solo mostrar esos
        where = Q(ejercicio__id=ejercicio_id)
        if ids is not None:
            where = where & Q(id__in=ids)
        if fecha_fin is not None:
            where = where & Q(
                registroasiento__asiento__imputacion__lte=fecha_fin)
        if tipo__in is not None:
            where = where & Q(
                registroasiento__asiento__tipo__in=tipo__in)
        # armo el listado de cuentas ordenado por el codigo y ya
        # sumarizando el saldo de los asientos
        cuentas = Cuenta.objects.filter(where).exclude(
            codigo="0.0.00.00.000").annotate(
                debitos=Sum("registroasiento__debe"),
                creditos=Sum("registroasiento__haber"),
                childs=Count("cuenta__id"))

        # filtro las cuentas hojas que no tienen uso
        if hide_inuse:
            cuentas = cuentas.filter(Q(childs__gt=0) |
                                     Q(debitos__isnull=False) |
                                     Q(creditos__isnull=False))
        cuentas = cuentas.order_by("codigo")
        # armo un vector donde poner la linea sucesoria de cada cuenta
        acuentas = [None] * 5

        # una lista para eliminar los que no tienen uso
        removeList = list()
        for c in cuentas:
            if c.debitos is None:
                c.debitos = Decimal(0)
            if c.creditos is None:
                c.creditos = Decimal(0)
            # i va a cambiar la cuenta chequeo que tenga uso de lo
            # contario la marco para sacarla
            if hide_inuse and acuentas[c.nivel] is not None and \
               acuentas[c.nivel].saldo == utils.PartidaDoble(0, 0):
                removeList.append(acuentas[c.nivel])
            acuentas[c.nivel] = c
            for i in range(0, c.nivel):
                if acuentas[i] is not None:
                    acuentas[i].debitos += c.debitos
                    acuentas[i].creditos += c.creditos

        lc = list(cuentas)

        if hide_inuse:
            # chequeo las cuentas que quedaron el el vector
            # sumarizador si tienen saldo
            for c in acuentas:
                if c is not None and c.saldo == utils.PartidaDoble(0, 0):
                    removeList.append(c)
            # quito las cuentas sin uso
            for c in removeList:
                lc.remove(c)

        return lc

    def __unicode__(self):
        return "%s %s" % (self.codigo, self.nombre)

    @staticmethod
    def NormalizeKey(key):
        """Toma un codigo y lo formatea (si puede) en un codigo de cuenta"""
        warnings.warn(
            'contable.models.Cuenta.NormalizeKey() is deprecated in '
            'favor of contable.models.CuentaKey.NormalizeKey().',
            RemovedInNextVersionWarning,
        )
        return CuentaKey.NormalizeKey(key)


class AsientoQuerySet(models.QuerySet):
    def delete(self, *args, **kwargs):
        for obj in self:
            obj.verificacion(*args, **kwargs)
        super(AsientoQuerySet, self).delete(*args, **kwargs)


class Asiento(models.Model):

    class Meta:
        permissions = (
            ("view_libro_diario", "Puede ver el libro diario"),
        )
    objects = AsientoQuerySet.as_manager()

    ejercicio = models.ForeignKey(Ejercicio)
    numero = models.PositiveIntegerField("número")
    imputacion = models.DateField()
    carga = models.DateField(null=True, blank=True)
    datos = utils.DictionaryField("Datos Comprobante", null=True, blank=True)
    descripcion = models.TextField("Descripción", null=True, blank=True)

    TIPO_ASIENTO = [(0, "Normal"), (1, "Apertura"),
                    (2, "Resultado"), (3, "Ajuste"), (4, "Cierre")]
    tipo = models.IntegerField("Tipo", choices=TIPO_ASIENTO, default=0)

    def __unicode__(self):
        return "%s %s - %s" % (self.numero, self.imputacion, self.descripcion)

    def verificacion(self):
        if self.registroasiento_set.all().count() <= 0:
            return False
        saldo = self.saldo
        if saldo.resultado != 0:
            return saldo.resultado
        return True

    def get_saldo(self, fecha_fin=None):
        saldo = utils.PartidaDoble(0, 0)
        if fecha_fin:
            ra = self.registroasiento_set.filter(
                asiento__imputacion_lte=fecha_fin)
        else:
            ra = self.registroasiento_set.all()
        for registro in ra:
            saldo += registro.saldo
        return saldo

    @property
    def saldo(self):
        return self.get_saldo()

    def save(self, *args, **kwargs):
        #   self.model_verification(*args, **kwargs)
        """si no le asigno numero, que asigne el suiguiente"""
        if self.numero is None:
            self.numero = self.ejercicio.get_next_asiento_numero()
        super(Asiento, self).save(*args, **kwargs)


class RegistroAsiento(models.Model):
    asiento = models.ForeignKey(Asiento)
    cuenta = models.ForeignKey(Cuenta, on_delete=models.PROTECT)
    datos = utils.DictionaryField(null=True, blank=True)
    debe = models.DecimalField(max_digits=20, decimal_places=2, blank=True)
    haber = models.DecimalField(max_digits=20, decimal_places=2, blank=True)

    # def __unicode__(self):
    #   return "%s %d" % (self.DEBEHABER_CHOICES[self.debehaber], self.monto)

    def save(self, *args, **kwargs):
        self.model_verification()
        super(RegistroAsiento, self).save(*args, **kwargs)

    def model_verification(self):
        if not self.cuenta.imputable:
            raise ValidationError({'cuenta': ('Esta cuenta no es imputable')})

        if self.debe is None:
            self.debe = Decimal(0)
        if self.haber is None:
            self.haber = Decimal(0)

        if self.debe == Decimal(0) and self.haber == Decimal(0):
            raise ValidationError(
                ('Se debe agregar un saldo en el debe o el haber'),
                code='required')

        if self.debe < 0:
            raise ValidationError({'debe': ('el monto no puede ser negativo')})
        if self.haber < 0:
            raise ValidationError(
                {'haber': ('el monto no puede ser negativo')})

        if self.debe > 0 and self.haber > 0:
            raise ValidationError(
                'No se puede ingresar valor en debe y haber juntos',
            )

    def clean(self, *args, **kwargs):
        self.model_verification()
        super(RegistroAsiento, self).clean(*args, **kwargs)

    @property
    def saldo(self):
        return utils.PartidaDoble(self.debe, self.haber)

    @property
    def isDebe(self):
        return (self.debe > 0)

    def __unicode__(self):
        return "Asiento %s (%s) - %s" % (self.asiento.numero,
                                         self.asiento.ejercicio.ano,
                                         self.cuenta)
