# -*- coding: utf-8 *-*
from django.conf.urls import url
import views

app_name = 'contable'
urlpatterns = [
    url(r'^plan_de_cuentas/(?P<ejercicio_id>\d+)/',
        views.PlanDeCuentas.as_crud(views.CuentaMixin,
                                    namespace='plan-de-cuentas')),
    url(r'^plan_de_cuentas/(?P<ejercicio_id>\d+)/copiar/$',
        views.copiar_plan_de_cuentas, name='copiar-plan-de-cuentas'),
    url(r'^plan_de_cuentas/(?P<ejercicio_id>\d+)/cierre/$',
        views.asiento_cierre, name='cierre-plan-de-cuentas'),
    url(r'^plan_de_cuentas/(?P<ejercicio_id>\d+)/apertura/$',
        views.asiento_apertura, name='apertura-plan-de-cuentas'),
    url(r'^plan_de_cuentas/(?P<ejercicio_id>\d+)/resultado/$',
        views.asiento_resultado, name='resultado-plan-de-cuentas'),
    url(r'^libro_mayor/(?P<cuenta_id>\d+)/',
        views.LibroMayor.as_view(namespace='libro-mayor')),
    url(r'^libro_diario/(?P<ejercicio_id>\d+)/',
        views.LibroDiario.as_view(namespace='libro-diario')),
    url(r'^ejercicio/',
        views.Ejercicio.as_crud(views.EjercicioMixin, namespace='ejercicio')),
    url(r'^asiento/update/(?P<asiento_id>\d+)/$',
        views.asiento_update, name="asiento-update"),
    url(r'^asiento/create/(?P<ejercicio_id>\d+)/$',
        views.asiento_create_new, name="asiento-create"),
    url(r'^asiento/delete/(?P<asiento_id>\d+)/$',
        views.asiento_delete, name="asiento-delete"),
    url(r'^asiento/resort/(?P<ejercicio_id>\d+)/$',
        views.asiento_resort, name="asiento-resort"),
    url(r'^asiento/move/(?P<asiento_id>\d+)/(?P<ejercicio_id>\d+)/$',
        views.asiento_move, name="asiento-move"),
    url(r'^plan_de_cuentas/(?P<ejercicio_id>\d+)/exportar/$',
        views.exportar_plan_de_cuentas, name='exportar-plan-de-cuentas'),
    url(r'^plan_de_cuentas/(?P<ejercicio_id>\d+)/importar/$',
        views.importar_plan_de_cuentas, name='importar-plan-de-cuentas'),
]
