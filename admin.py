# -*- coding: utf-8 *-*
import models
from django.contrib import admin
registros_basicos = (

    models.Ejercicio,

)


class CuentaAdmin(admin.ModelAdmin):
    list_display = ('codigo', 'nombre', 'imputable')
    list_display_links = ('codigo', 'nombre')
    list_filter = ('imputable', 'ejercicio')
    search_fields = ('codigo', 'nombre')


class RegistroAsientoInLine(admin.StackedInline):

    model = models.RegistroAsiento

    fields = (
        ('cuenta'),
        ('debe', 'haber'),

    )


class AsientoAdmin(admin.ModelAdmin):
    list_display = ('ejercicio', 'numero', 'imputacion',
                    'verificacion', 'descripcion')
    list_display_links = ('ejercicio', 'numero')
    list_filter = ('ejercicio',)
    search_fields = ('descripcion', 'datos', 'numero', 'imputacion')

    inlines = (RegistroAsientoInLine,)


for registro in registros_basicos:
    admin.site.register(registro)

admin.site.register(models.Cuenta, CuentaAdmin)
admin.site.register(models.Asiento, AsientoAdmin)
