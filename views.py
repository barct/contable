# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from datetime import datetime

from django.db.models import Sum, Q
from utils import PartidaDoble

from generic_views.views import ListView, Breadcrumb, MixinView, CreateView
# from generic_views.manager import register_manager
from django.forms import inlineformset_factory
from django.forms import modelform_factory
from generic_views import widgets
from django.contrib.auth.decorators import permission_required
from django.core.exceptions import ValidationError, ObjectDoesNotExist
# from generic_views.libs.debug import oprint
from django.db import transaction
import textwrap

import models
import forms
import json
import re


def parse_date(value):
    m = re.search('^(\d{4})(\d{2})(\d{2})$', value)
    return datetime(int(m.group(1)), int(m.group(2)), int(m.group(3)))


class LibroDiario(ListView):

    module_name = "contable"
    page_title = "Libro Diario"

    search_fields = ('descripcion', 'numero',
                     'registroasiento__cuenta__nombre',
                     'datos')

    filter_fields = ('tipo',)

    table_list_template_name = 'contable/libro_diario_table.html'

    template_ods_name = "contable/libro_diario_ods.html"
    template_print_name = "contable/libro_diario_print.html"
    permissions = "contable.view_libro_diario"

    def get(self, request, ejercicio_id, *args, **kwargs):
        self.ejercicio_id = ejercicio_id
        self.objects_list = models.Asiento.objects.filter(
            ejercicio_id=ejercicio_id).order_by("numero")
        self.page_title = "Libro Diario " + \
            str(models.Ejercicio.objects.get(pk=ejercicio_id).ano)

        self.configFechaFilter(request)
        return super(LibroDiario, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        context['ejercicio_id'] = self.ejercicio_id
        context['ejercicio'] = models.Ejercicio.objects.get(
            pk=self.ejercicio_id)

        if self.export == "print":
            lines = list()

            transporte = PartidaDoble()
            for asiento in self.objects_list:
                lines.append((asiento.imputacion, asiento.numero, None, None,
                              "asiento_numero", transporte.debe,
                              transporte.haber))
                ra_set = asiento.registroasiento_set.all().order_by(
                    "cuenta__codigo")
                for ra in ra_set.filter(debe__gt=0):
                    transporte += ra.saldo
                    lines.append((None, "%s - %s" % (ra.cuenta.codigo,
                                                     ra.cuenta.nombre),
                                  ra.debe, None, "registroasiento_debe",
                                  transporte.debe, transporte.haber))

                for ra in ra_set.filter(haber__gt=0):
                    transporte += ra.saldo
                    lines.append((None, "%s - %s" % (ra.cuenta.codigo,
                                                     ra.cuenta.nombre),
                                  None, ra.haber, "registroasiento_haber",
                                  transporte.debe, transporte.haber))

                desc_lines = textwrap.wrap(asiento.descripcion, 60)
                for dl in desc_lines:
                    lines.append((None, dl, None, None,
                                  "asiento_descripcion",
                                  transporte.debe, transporte.haber))
            context['lines'] = lines
        return context

    def configFechaFilter(self, request):
        # request fechas
        desde = request.GET.get("fd", None)
        hasta = request.GET.get("fh", None)
        if desde:
            try:
                desde = parse_date(desde)
            except ValueError:
                desde = None
        if hasta:
            try:
                hasta = parse_date(desde)
            except ValueError:
                hasta = None

        if desde is not None:
            self.filter = Q(imputacion__gte=desde)
            self.keep_var('fd', request.GET.get("fd"))
            if hasta is None:
                self.filter = self.filter & Q(imputacion__lte=desde)
                self.page_subtitle = "del día %s" % desde.strftime('%d, %b %Y')
            else:
                self.filter = self.filter & Q(imputacion__lte=hasta)
                self.page_subtitle = "del día %s al %s" % (
                    desde.strftime('%d, %b %Y'), hasta.strftime('%d, %b %Y'))
                self.keep_var('fh', request.GET.get("fh"))

    @staticmethod
    def get_menu():
        submenu = list()
        ejercicios = models.Ejercicio.objects.all()
        for e in ejercicios:
            submenu.append({'label': str(e.ano),
                            'url': '/contable/libro_diario/' +
                            str(e.id) + "/list/",
                            })
        return {
            'label': "Libro Diario",
            'menu': submenu,
            'permissions': 'contable.view_libro_diario',
        }

    def get_breadcrumbs(self, request):
        bc = super(LibroDiario, self).get_breadcrumbs(request)
        bc.append(Breadcrumb("Ejercicio " + str(self.ejercicio_id)))
        return bc


class CuentaMixin(MixinView):
    model = models.Cuenta
    fields = ("parent", "codigo", "breve", "nombre", "imputable")

    permissions_create = "contable.add_cuenta"
    permissions_change = "contable.change_cuenta"
    permissions_delete = "contable.delete_cuenta"

    fields_propertys = (
        ('parent', {
            'widget': widgets.TreeView(
                # queryset=parent_queryset, ##lo especifico despues porque
                # hay que filtrar ejercicio
                parent_field="parent_id",
            ),
            'verbose_name': "Padre",
        }),
    )

    def render_to_response(self, form, *args, **kwargs):
        # filtro ejercicio en el treeview del parent
        if "form" in form:
            ejercicio_id = self.kwargs["ejercicio_id"]
            w = form["form"].fields["parent"].widget
            w.queryset = models.Cuenta.objects.filter(
                ejercicio_id=ejercicio_id)
        return super(CuentaMixin, self).render_to_response(form,
                                                           *args,
                                                           **kwargs)

    def form_valid(self, form, *args, **kwargs):
        # seteo el ejercicio
        ejercicio_id = self.kwargs["ejercicio_id"]
        form.instance.ejercicio_id = ejercicio_id
        return super(CuentaMixin, self).form_valid(form, *args, **kwargs)


class PlanDeCuentas(ListView):
    module_name = "contable"

    list_display = ("cuenta, saldo")

    permissions = "contable.view_plan_de_cuentas"

    def get(self, request, ejercicio_id, *args, **kwargs):
        ejercicio = models.Ejercicio.objects.get(pk=ejercicio_id)
        self.page_title = "Plan de Cuentas " + str(ejercicio.ano)

        self.ejercicio_id = ejercicio_id
        hide_inusados = False
        cuentas = models.Cuenta.listadoCompleto(ejercicio_id, hide_inusados)

        template = loader.get_template('contable/plan_de_cuentas.html')
        context = {
            'page_title': self.page_title,
            'cuentas': cuentas,
            'hide_inusados': hide_inusados,
            'ejercicio': ejercicio,
        }
        return HttpResponse(template.render(context, request))

    @staticmethod
    def get_menu():
        submenu = list()
        ejercicios = models.Ejercicio.objects.all()
        for e in ejercicios:
            submenu.append({
                'label': str(e.ano),
                'url': '/contable/plan_de_cuentas/' + str(e.id) + "/list/", })
        return {
            'label': "Plan de Cuentas",
            'menu': submenu,
            'permissions': 'contable.view_plan_de_cuentas',
        }

    def get_breadcrumbs(self, request):
        bc = super(PlanDeCuentas, self).get_breadcrumbs(request)
        bc.append("Ejercicio " + str(self.ejercicio_id))
        return bc


class LibroMayor(ListView):
    module_name = "contable"
    page_title = "Libro Mayor"
    table_list_template_name = 'contable/libro_mayor_table.html'

    permissions = 'contable.view_plan_de_cuentas'

    def get(self, request, cuenta_id, *args, **kwargs):

        self.cuenta = models.Cuenta.objects.get(id=cuenta_id)

        self.ids = self.cuenta.get_rel_ids()
        self.objects_list = self.cuenta.get_movimientos().order_by(
            'asiento__numero', 'asiento__imputacion', 'cuenta__id')

        self.page_title = "Libro Mayor " + \
            str(models.Ejercicio.objects.get(pk=self.cuenta.ejercicio_id).ano)
        self.page_subtitle = unicode(self.cuenta.nombre)

        return super(LibroMayor, self).get(request, *args, **kwargs)

    def get_breadcrumbs(self, request):
        bc = super(LibroMayor, self).get_breadcrumbs(request)
        bc.append(Breadcrumb(unicode(self.cuenta.nombre) +
                             " " + unicode(self.cuenta.codigo)))
        return bc

    def postprocess_get(self, request, context):
        start = self.objects_page.start_index() - 1
        end = self.objects_page.end_index()

        page = models.RegistroAsiento.objects.filter(
            cuenta_id__in=self.ids).order_by(
            'asiento__numero', 'asiento__imputacion', 'cuenta__id')

        if (start > 0):
            transporte_start = page[:start].annotate().aggregate(
                debe=Sum("debe"), haber=Sum("haber"))
            context["transporte_start"] = PartidaDoble(
                transporte_start["debe"], transporte_start["haber"])
        transporte_end = page[:end].annotate().aggregate(
            debe=Sum("debe"), haber=Sum("haber"))

        context["transporte_end"] = PartidaDoble(
            transporte_end["debe"], transporte_end["haber"])

    def configPaginator(self, request):
        saldo_acumulado = 0
        for a in self.objects_list:
            saldo_acumulado += a.saldo.resultado
            a.saldo_acumulado = saldo_acumulado
            if saldo_acumulado < 0:
                a.fontcolor = "#F00"

        super(LibroMayor, self).configPaginator(request)


class EjercicioCreateView(CreateView):
    model = models.Ejercicio
    form_class = forms.EjercicioForm

    def form_valid(self, form):
        return super(EjercicioCreateView, self).form_valid(form)


class EjercicioMixin(MixinView):
    module_name = "contable"
    model = models.Ejercicio
    fields = ['id', 'ano', 'fecha_inicio', 'fecha_fin', 'activo']

    permissions_create = 'contable.add_ejercicio'
    permissions_change = 'contable.change_ejercicio'
    permissions_delete = 'contable.delete_ejercicio'


class Ejercicio(ListView):
    module_name = "contable"
    page_title = "Ejercicio"
    objects_list = models.Ejercicio.objects.all()
    list_display = ("ano", "fecha_inicio", "fecha_fin", "activo")

    permissions = 'contable.view_ejercicio'

    @staticmethod
    def get_menu():
        return {
            'label': "Ejercicios",
            'url': '/contable/ejercicio/list/',
            'permissions': 'contable.view_ejercicio',
        }


@permission_required('contable.change_asiento', raise_exception=True)
def asiento_update(request, asiento_id):
    asiento = models.Asiento.objects.get(pk=asiento_id)
    Form = modelform_factory(models.Asiento, form=forms.AsientoForm)
    registro_asiento_formclass = forms.RegistroAsientoForm
    registro_asiento_formclass.ejercicio_id = asiento.ejercicio_id
    FormSet = inlineformset_factory(models.Asiento, models.RegistroAsiento,
                                    form=registro_asiento_formclass,
                                    formset=forms.RegistroAsientoFormSet)

    if request.method == 'POST':
        form = Form(data=request.POST, instance=asiento, request=request)
        formset = FormSet(request.POST, instance=asiento)
        if form.is_valid() and formset.is_valid():
            asiento = form.save()
            formset.save()
            return redirect(
                "%s?id=%d" % (reverse("contable:libro-diario:list",
                                      args={asiento.ejercicio_id}), asiento.pk)
            )

    else:
        form = Form(instance=asiento, request=request)
        formset = FormSet(instance=asiento)
    return render(request, 'contable/forms/asiento_form.html', {
        'page_title': "Editando de Asiento N° " + str(asiento.numero) +
        " Ejercicio " + str(asiento.ejercicio.ano),
        'form': form,
        'formset': formset,
        'ejercicio': asiento.ejercicio
    })


@permission_required('contable.add_asiento', raise_exception=True)
def asiento_resultado(request, ejercicio_id):
    CUENTA_INGRESOS = "4.0.00.00.000"
    CUENTA_EGRESOS = "5.0.00.00.000"
    fs_initial = {}
    form_initial = {}
    if not request.method == 'POST':
        ejercicio = models.Ejercicio.objects.get(pk=ejercicio_id)
        form_initial = {
            "imputacion": ejercicio.fecha_fin,
            "tipo": 2,
            "descripcion": "Resultado del Ejercicio %s" % ejercicio.ano
        }
        index = 0

        cuentas_ingresos = models.Cuenta.objects.get(
            ejercicio_id=ejercicio_id, codigo=CUENTA_INGRESOS).get_rel_ids()
        cuentas_gastos = models.Cuenta.objects.get(
            ejercicio_id=ejercicio_id, codigo=CUENTA_EGRESOS).get_rel_ids()

        cuentas = models.Cuenta.objects.filter(
            pk__in=cuentas_ingresos + cuentas_gastos).order_by("codigo")

        for cuenta in sorted(cuentas,
                             key=lambda obj:
                             (obj.saldo.debe > obj.saldo.haber)):
            if cuenta.saldo.resultado == 0.0:
                continue
            fs_initial["registroasiento_set-%s-cuenta" % index] = cuenta.id
            fs_initial["registroasiento_set-%s-id" % index] = ""
            fs_initial["registroasiento_set-%s-asiento" % index] = ""

            resultado = cuenta.saldo.resultado
            fs_initial["registroasiento_set-%s-debe" % index] = 0
            fs_initial["registroasiento_set-%s-haber" % index] = 0
            if resultado > 0:
                fs_initial["registroasiento_set-%s-haber" % index] = resultado
            else:
                fs_initial["registroasiento_set-%s-debe" % index] = -resultado
            index += 1

        fs_initial["registroasiento_set-INITIAL_FORMS"] = 0
        fs_initial["registroasiento_set-TOTAL_FORMS"] = index + 1

    return asiento_create(request=request,
                          ejercicio_id=ejercicio_id,
                          form_initial=form_initial,
                          formset_initial=fs_initial)


@permission_required('contable.add_asiento', raise_exception=True)
def asiento_apertura(request, ejercicio_id):
    fs_initial = {}
    form_initial = {}
    if not request.method == 'POST':
        ejercicio = models.Ejercicio.objects.get(pk=ejercicio_id)
        form_initial = {
            "imputacion": ejercicio.fecha_inicio,
            "tipo": 1,
            "descripcion": "Apertura Ejercicio %s" % ejercicio.ano,
            "numero": 1,
        }
        index = 0
        try:
            ejercicio_ant = models.Ejercicio.objects.get(ano=ejercicio.ano - 1)
        except ObjectDoesNotExist:
            return redirect(
                reverse("contable:asiento-create", args={ejercicio_id}))

        asiento_cierre = ejercicio_ant.asiento_set.all().order_by("-numero")[0]

        for ra in sorted(asiento_cierre.registroasiento_set.all(),
                         key=lambda obj: (obj.debe > obj.haber)):
            try:
                cuenta_id = models.Cuenta.objects.get(
                    ejercicio_id=ejercicio_id, codigo=ra.cuenta.codigo)
            except ObjectDoesNotExist:
                continue
            fs_initial["registroasiento_set-%s-cuenta" % index] = cuenta_id.id
            fs_initial["registroasiento_set-%s-id" % index] = ""
            fs_initial["registroasiento_set-%s-asiento" % index] = ""

            fs_initial["registroasiento_set-%s-debe" % index] = ra.haber
            fs_initial["registroasiento_set-%s-haber" % index] = ra.debe
            index += 1

        fs_initial["registroasiento_set-INITIAL_FORMS"] = 0
        fs_initial["registroasiento_set-TOTAL_FORMS"] = index + 1

    return asiento_create(request=request,
                          ejercicio_id=ejercicio_id,
                          form_initial=form_initial,
                          formset_initial=fs_initial)


@permission_required('contable.add_asiento', raise_exception=True)
def asiento_cierre(request, ejercicio_id, *args, **kwargs):
    CUENTA_ACTIVO = "1.0.00.00.000"
    CUENTA_PASIVO = "2.0.00.00.000"
    CUENTA_PATRIMONIO = "3.0.00.00.000"

    fs_initial = {}
    form_initial = {}
    if not request.method == 'POST':
        ejercicio = models.Ejercicio.objects.get(pk=ejercicio_id)
        form_initial = {
            "imputacion": ejercicio.fecha_fin,
            "tipo": 4,
            "descripcion": "Cierre de Ejercicio %s" % ejercicio.ano
        }
        index = 0

        cuentas_activo = models.Cuenta.objects.get(
            ejercicio_id=ejercicio_id, codigo=CUENTA_ACTIVO).get_rel_ids()
        cuentas_pasivo = models.Cuenta.objects.get(
            ejercicio_id=ejercicio_id, codigo=CUENTA_PASIVO).get_rel_ids()
        cuentas_patrimonio = models.Cuenta.objects.get(
            ejercicio_id=ejercicio_id, codigo=CUENTA_PATRIMONIO).get_rel_ids()

        cuentas = models.Cuenta.objects.filter(
            pk__in=cuentas_activo +
            cuentas_pasivo +
            cuentas_patrimonio).order_by("codigo")

        for cuenta in sorted(cuentas, key=lambda obj:
                             (obj.saldo.debe > obj.saldo.haber)):
            if cuenta.saldo.resultado == 0.0:
                continue
            fs_initial["registroasiento_set-%s-cuenta" % index] = cuenta.id
            fs_initial["registroasiento_set-%s-id" % index] = ""
            fs_initial["registroasiento_set-%s-asiento" % index] = ""

            resultado = cuenta.saldo.resultado
            fs_initial["registroasiento_set-%s-debe" % index] = 0
            fs_initial["registroasiento_set-%s-haber" % index] = 0
            if resultado > 0:
                fs_initial["registroasiento_set-%s-haber" % index] = resultado
            else:
                fs_initial["registroasiento_set-%s-debe" % index] = -resultado
            index += 1

        fs_initial["registroasiento_set-INITIAL_FORMS"] = 0
        fs_initial["registroasiento_set-TOTAL_FORMS"] = index + 1

    return asiento_create(request=request,
                          ejercicio_id=ejercicio_id,
                          form_initial=form_initial,
                          formset_initial=fs_initial)


@permission_required('contable.add_asiento', raise_exception=True)
def asiento_create_new(request, ejercicio_id):
    return asiento_create(request=request,
                          ejercicio_id=ejercicio_id,
                          form_initial={},
                          formset_initial={})


def asiento_create(request, ejercicio_id, form_initial={}, formset_initial={}):
    ejercicio = models.Ejercicio.objects.get(pk=ejercicio_id)
    if ejercicio.cuenta_set.count() <= 0:
        raise "Error: No hay plan de cuentas"
    if "imputacion" not in form_initial:
        form_initial["imputacion"] = datetime.now
    if "numero" not in form_initial:
        form_initial["numero"] = ejercicio.get_next_asiento_numero()
    if "ejercicio" not in form_initial:
        form_initial["ejercicio"] = ejercicio
    if "tipo" not in form_initial:
        form_initial["tipo"] = 0

    Form = modelform_factory(models.Asiento, form=forms.AsientoForm)
    registro_asiento_formclass = forms.RegistroAsientoForm
    registro_asiento_formclass.ejercicio_id = ejercicio_id
    FormSet = inlineformset_factory(models.Asiento, models.RegistroAsiento,
                                    form=registro_asiento_formclass,
                                    formset=forms.RegistroAsientoFormSet)

    if request.method == 'POST':
        form = Form(data=request.POST, request=request)
        formset = FormSet(request.POST)
        if form.is_valid() and formset.is_valid():
            asiento = form.save(commit=False)
            # asiento.ejercicio = ejercicio
            asiento.save()
            formset.instance = asiento
            formset.save()
            return redirect(
                "%s?id=%d" % (reverse("contable:libro-diario:list",
                                      args={asiento.ejercicio_id}), asiento.pk)
            )
    else:
        form = Form(initial=form_initial, request=request)
        if len(formset_initial) > 0:
            formset = FormSet(formset_initial)
        else:
            formset = FormSet()
    return render(request, 'contable/forms/asiento_form.html', {
        'page_title': "Agregando nuevo asiento en ejercicio " +
        str(ejercicio.ano),
        'form': form,
        'formset': formset,
        'ejercicio_id': ejercicio
    })


@permission_required('contable.delete_asiento', raise_exception=True)
def asiento_delete(request, asiento_id):
    asiento = models.Asiento.objects.get(pk=asiento_id)
    is_superuser = request.user.is_superuser
    if models.Ejercicio.get_activo() != asiento.ejercicio and not is_superuser:
        raise ValidationError('El ejercicio no está activo')
    if request.method == 'POST':
        if request.POST.get("confirm", False):
            asiento.delete()
            return redirect(reverse("contable:libro-diario:list",
                                    args={asiento.ejercicio_id}))
        else:
            return redirect(
                "%s?id=%d" % (reverse("contable:libro-diario:list",
                                      args={asiento.ejercicio_id}), asiento.pk)
            )
    return render(request, 'contable/forms/asiento_delete.html',
                  {'objects_page': (asiento,)})


@permission_required('contable.change_asiento', raise_exception=True)
def asiento_resort(request, ejercicio_id):
    ejercicio = models.Ejercicio.objects.get(pk=ejercicio_id)
    ejercicio.resort_asiento_numero()
    return redirect(reverse("contable:libro-diario:list", args={ejercicio_id}))


@permission_required('contable.add_cuenta', raise_exception=True)
@transaction.atomic
def copiar_plan_de_cuentas(request, ejercicio_id):
    ejercicio_to = models.Ejercicio.objects.get(pk=ejercicio_id)
    if request.method == 'POST':
        form = forms.CopiarPlanDeCuentas(
            data=request.POST, ejercicio_id=ejercicio_to.id)
        if form.is_valid():
            ejercicio_from = form.cleaned_data["ejercicio_from"]
            for cuenta_from in ejercicio_from.cuenta_set.all():
                try:
                    cuenta_to = ejercicio_to.cuenta_set.get(
                        codigo=cuenta_from.codigo)
                except ObjectDoesNotExist:
                    cuenta_to = models.Cuenta()

                cuenta_to.codigo = cuenta_from.codigo
                cuenta_to.breve = cuenta_from.breve
                cuenta_to.nombre = cuenta_from.nombre
                cuenta_to.imputable = cuenta_from.imputable
                cuenta_to.ejercicio = ejercicio_to

                cuenta_to.save()

                content = '{"status": "OK"}'
                response = HttpResponse(
                    content_type='application/json; charset=utf-8',
                    content=content)
                return response
    else:
        form = forms.CopiarPlanDeCuentas(ejercicio_id=ejercicio_to.id)

    return render(request, 'generic_views/form_view.html', {
        'page_title': "Copiar cuentas al ejercicio " + str(ejercicio_to),
        'form': form,
    })


@permission_required('contable.add_cuenta', raise_exception=True)
def exportar_plan_de_cuentas(request, ejercicio_id):
    ejercicio = models.Ejercicio.objects.get(pk=ejercicio_id)
    cuentas = models.Cuenta.objects.filter(
        ejercicio=ejercicio).order_by("codigo")
    data = render(request, 'contable/plan_cuentas_export.html', {
        'cuentas': cuentas,
    })
    response = HttpResponse(data, content_type='application/json')
    response['Content-Disposition'] = (
        'attachment; filename=PlanDeCuanta_Ej%s.json' % ejercicio.ano)
    response['Content-Length'] = len(data.content)
    return response


@permission_required('contable.add_cuenta', raise_exception=True)
def importar_plan_de_cuentas(request, ejercicio_id):
    ejercicio_to = models.Ejercicio.objects.get(pk=ejercicio_id)
    if request.method == 'POST':
        form = forms.ImportarPlanCuentas(request.POST, request.FILES)
        if form.is_valid():
            json_file = form.cleaned_data["json_file"]
            cuentas = json.loads(json_file.read())

            for cuenta_from in cuentas:
                try:
                    cuenta_to = ejercicio_to.cuenta_set.get(
                        codigo=cuenta_from["codigo"])
                except ObjectDoesNotExist:
                    cuenta_to = models.Cuenta()

                cuenta_to.codigo = cuenta_from["codigo"]
                cuenta_to.breve = cuenta_from["breve"]
                cuenta_to.nombre = cuenta_from["nombre"]
                cuenta_to.imputable = cuenta_from["imputable"]
                cuenta_to.ejercicio = ejercicio_to

                cuenta_to.full_clean()
                cuenta_to.save()

            content = '{"status": "OK"}'
            response = HttpResponse(
                content_type='application/json; charset=utf-8',
                content=content)
            return response
    else:
        form = forms.ImportarPlanCuentas()

    return render(request, 'generic_views/form_view.html', {
        'page_title': "Importar Plan de Cuentas " + str(ejercicio_to),
        'form': form,
    })


@permission_required('contable.change_asiento', raise_exception=True)
@transaction.atomic
def asiento_move(request, asiento_id, ejercicio_id):
    ejercicio = models.Ejercicio.objects.get(pk=ejercicio_id)
    asiento = models.Asiento.objects.get(pk=asiento_id)
    for ra in asiento.registroasiento_set.all():
        cuenta = ejercicio.cuenta_set.get(codigo=ra.cuenta.codigo)
        ra.cuenta = cuenta
        ra.save()

    asiento.ejercicio = ejercicio
    asiento.save()
    return redirect(reverse("contable:libro-diario:list", args={ejercicio_id}))
