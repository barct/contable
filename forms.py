# -*- encoding: utf-8 -*-
from django import forms
import models
from generic_views import widgets
from django.forms import BaseInlineFormSet
from contable.utils import PartidaDoble
from django.core.exceptions import ValidationError


class RegistroAsientoForm(forms.ModelForm):
    class Meta:
        model = models.RegistroAsiento
        fields = ("asiento", "cuenta", "datos", "debe", "haber",)
        widgets = {
            'debe': forms.TextInput(attrs={'style': 'direction: rtl;'}),
            'haber': forms.TextInput(attrs={'style': 'direction: rtl;'}),
        }

    def __init__(self, *args, **kwargs):
        super(RegistroAsientoForm, self).__init__(*args, **kwargs)
        if not hasattr(self, "ejercicio_id"):
            raise Exception(
                "Error de configuración. Debe especificar el ejercicio")

        self.fields["cuenta"].widget = widgets.TreeView(
            queryset=self.fields["cuenta"].queryset.filter(
                ejercicio_id=self.ejercicio_id),
            parent_field='parent_id',
            unique_js_var='cuenta_tree',
            selecteable_field='imputable',)


class AsientoForm(forms.ModelForm):
    class Meta:
        model = models.Asiento
        fields = ("numero", "tipo", "imputacion", "carga",
                  "datos", "descripcion", "ejercicio")
        widgets = {
            "imputacion": widgets.DatePicker,
            "ejercicio": forms.HiddenInput
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        return super(AsientoForm, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        is_superuser = self.request.user.is_superuser
        if (models.Ejercicio.get_activo() != self.cleaned_data["ejercicio"] and
                not is_superuser):
            raise ValidationError('El ejercicio no está activo')


class RegistroAsientoFormSet(BaseInlineFormSet):

    def get_queryset(self):
        qs = super(RegistroAsientoFormSet, self).get_queryset()
        return sorted(qs, key=lambda obj: (obj.debe < obj.haber))

    def clean(self):
        forms = [f for f in self.forms
                 if f.cleaned_data and
                 # This next line filters out inline objects that did exist
                 # but will be deleted if we let this form validate --
                 # obviously we don't want to count those if our goal is to
                 # enforce a min or max number of related objects.
                 not f.cleaned_data.get('DELETE', False)
                 ]

        if len(forms) < 2:
            raise ValidationError("Debe haber al menos 2 cuentas")

        saldo = PartidaDoble()
        for form in forms:
            if form.is_valid():
                saldo += PartidaDoble(form.cleaned_data["debe"],
                                      form.cleaned_data["haber"])

        if saldo.resultado != 0:
            raise ValidationError(
                ("El asiento no está balanceado saldo: %(value)s"),
                code='invalid',
                params={"value": saldo.resultado},)


class CopiarPlanDeCuentas(forms.Form):
    ejercicio_from = forms.ModelChoiceField(
        queryset=models.Ejercicio.objects.all(), label="Ejercicio Origen")

    def __init__(self, ejercicio_id, *args, **kwargs):
        super(CopiarPlanDeCuentas, self).__init__(*args, **kwargs)
        self.fields["ejercicio_from"].queryset = self.fields[
            "ejercicio_from"].queryset.exclude(id=ejercicio_id)


class EjercicioForm(forms.ModelForm):
    class Meta:
        model = models.Ejercicio
        fields = ['id', 'ano', 'fecha_inicio', 'fecha_fin', 'activo']

    name = forms.FileField(required=False)


class ImportarPlanCuentas(forms.Form):
    json_file = forms.FileField("Archivo JSON")
