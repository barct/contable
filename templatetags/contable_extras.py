# -*- coding: utf-8 -*-
from django.template import Library
from django.utils.formats import number_format
import re
import json
from decimal import Decimal, InvalidOperation
from contable import utils
from django.db.models import Sum

register = Library()


@register.filter_function
def order_by(queryset, args):
    args = [x.strip() for x in args.split(',')]
    qs = queryset.order_by(*args)
    return qs


@register.filter_function
def filter(queryset, args):
    largs = [x.strip() for x in args.split(',')]
    args = dict()
    for a in largs:
        la = a.split("=")
        args[la[0]] = la[1]
    qs = queryset.filter(**args)
    return qs


@register.filter_function
def sum(queryset):
    qs = queryset.aggregate(Sum("debe"), Sum("haber"))
    pd = utils.PartidaDoble(qs["debe__sum"], qs["haber__sum"])
    return pd


@register.filter_function
def absolute(value):
    return abs(value)


@register.inclusion_tag('contable/datos_asiento.html')
def datos_asiento(value):
    """
    datos_asiento pone bonito los datos extras de un
    asiento contable que vienen jsoneados
    """
    try:
        obj_datos = json.loads(value)
    except TypeError:
        return value

    for o in obj_datos:
        if isinstance(obj_datos[o], (tuple, list, set)):
            return({'label': o, 'data': datos_asiento(obj_datos[o])})
        else:
            return datos_asiento(obj_datos[o])


"""
format_dinero pone puntos en los miles y comma más 2 decimales
"""


@register.filter
def format_dinero(value):
    try:
        value = Decimal(value)
    except InvalidOperation:
        return ""

    value = number_format(value, 2, force_grouping=True)
    output = re.sub(r'(,)', '.', value)
    groups = output.split(".")
    decimal_part = groups.pop()
    int_part = ".".join(groups)
    return int_part + "," + decimal_part


@register.filter
def hideZero(value):
    try:
        if value is None or value == Decimal(0):
            return ""
    except ValueError:
        return ""
    return value


@register.filter
def urldate(date):
    output = str(date.year)
    output += str(date.month).zfill(2)
    output += str(date.day).zfill(2)
    return output


@register.inclusion_tag('contable/plan_de_cuentas_line.html')
def renderCuentaLine(cuenta):
    if cuenta.saldo.resultado < 0:
        font_color = "#F00"
    else:
        font_color = "#000"

    return {
        'cuentas': cuenta.cuenta_set.all(),
        'cuenta': cuenta,
        'saldo': cuenta.saldo,
        'nivelrange': range(cuenta.nivel),
        'font_size': 15 - cuenta.nivel,
        'font_color': font_color,
        'nivel': cuenta.nivel
    }
