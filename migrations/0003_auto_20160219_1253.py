# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contable', '0002_ejercicio_activo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registroasiento',
            name='debe',
            field=models.DecimalField(max_digits=20, decimal_places=2, blank=True),
        ),
        migrations.AlterField(
            model_name='registroasiento',
            name='haber',
            field=models.DecimalField(max_digits=20, decimal_places=2, blank=True),
        ),
    ]
