# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contable', '0006_asiento_tipo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asiento',
            name='tipo',
            field=models.IntegerField(default=0, verbose_name=b'Tipo', choices=[(0, b'Normal'), (1, b'Apertura'), (2, b'Resultado'), (3, b'Ajuste'), (4, b'Cierre')]),
        ),
    ]
