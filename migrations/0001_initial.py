# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Asiento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numero', models.PositiveIntegerField(verbose_name=b'n\xc3\xbamero')),
                ('imputacion', models.DateField()),
                ('carga', models.DateField(null=True, blank=True)),
                ('datos', models.TextField(null=True, verbose_name=b'Datos Comprobante', blank=True)),
                ('descripcion', models.TextField(null=True, verbose_name=b'Descripci\xc3\xb3n', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Cuenta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('codigo', models.CharField(max_length=13, verbose_name=b'C\xc3\xb3digo')),
                ('breve', models.CharField(max_length=5, null=True, verbose_name=b'Acceso R\xc3\xa1pido', blank=True)),
                ('nombre', models.CharField(max_length=127, verbose_name=b'Nombre')),
                ('imputable', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ejercicio',
            fields=[
                ('id', models.PositiveIntegerField(serialize=False, verbose_name=b'N\xc3\xbamero', primary_key=True)),
                ('fecha_inicio', models.DateField(verbose_name=b'Fecha de inicio')),
                ('fecha_fin', models.DateField(verbose_name=b'Fecha de final')),
                ('ano', models.PositiveIntegerField(verbose_name=b'A\xc3\xb1o')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegistroAsiento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datos', models.TextField(null=True, blank=True)),
                ('debe', models.DecimalField(max_digits=20, decimal_places=2)),
                ('haber', models.DecimalField(max_digits=20, decimal_places=2)),
                ('asiento', models.ForeignKey(to='contable.Asiento')),
                ('cuenta', models.ForeignKey(to='contable.Cuenta', on_delete=django.db.models.deletion.PROTECT)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='cuenta',
            name='ejercicio',
            field=models.ForeignKey(to='contable.Ejercicio'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cuenta',
            name='parent',
            field=models.ForeignKey(blank=True, to='contable.Cuenta', null=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='cuenta',
            unique_together=set([('codigo', 'ejercicio')]),
        ),
        migrations.AddField(
            model_name='asiento',
            name='ejercicio',
            field=models.ForeignKey(to='contable.Ejercicio'),
            preserve_default=True,
        ),
    ]
