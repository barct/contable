# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contable', '0005_auto_20160313_0824'),
    ]

    operations = [
        migrations.AddField(
            model_name='asiento',
            name='tipo',
            field=models.IntegerField(default=0, verbose_name=b'Tipo', choices=[(b'0', b'Normal'), (b'1', b'Apertura'), (b'2', b'Resultado'), (b'3', b'Ajuste'), (b'4', b'Cierre')]),
        ),
    ]
