# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-31 22:45
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contable', '0007_auto_20170830_2345'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ejercicio',
            options={'permissions': (('view_ejercicio', 'Puede ver los ejercicios'),)},
        ),
    ]
