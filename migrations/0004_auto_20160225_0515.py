# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import contable.utils


class Migration(migrations.Migration):

    dependencies = [
        ('contable', '0003_auto_20160219_1253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asiento',
            name='datos',
            field=contable.utils.DictionaryField(null=True, verbose_name=b'Datos Comprobante', blank=True),
        ),
        migrations.AlterField(
            model_name='registroasiento',
            name='datos',
            field=contable.utils.DictionaryField(null=True, blank=True),
        ),
    ]
