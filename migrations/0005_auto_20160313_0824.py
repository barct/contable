# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contable', '0004_auto_20160225_0515'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='asiento',
            options={'permissions': (('view_libro_diario', 'Puede ver el libro diario'),)},
        ),
        migrations.AlterModelOptions(
            name='cuenta',
            options={'permissions': (('view_plan_de_cuentas', 'Puede ver los planes de cuentas'),)},
        ),
    ]
